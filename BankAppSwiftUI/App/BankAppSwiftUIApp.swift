//
//  BankAppSwiftUIApp.swift
//  BankAppSwiftUI
//
//  Created by Jose Martinez on 8/20/20.
//

import SwiftUI

@main
struct BankAppSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(selectedCard: cards[0])
        }
    }
}
