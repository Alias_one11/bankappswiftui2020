import Foundation
import SwiftUI

class CardManager: ObservableObject {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    let currentDateStr: String = "17 Oct 2020"
    var lastTransactionID: String = UUID().uuidString
    var lastSelectedExpense = -1
    @Published var expensesValue = expenses
    /*..............................*/
    
    /// - ©Class methods
    /*©---------------------------------©*/
    
    // MARK: -∂ getUniqueDates
    func getUniqueDates(for cardNumber: String) -> [String] {
        //__________
        var dates: [String] = []
        var lastDate: String = ""
        
        for transaction in transactions {
            //__________
            if transaction.card == cardNumber && transaction.date != lastDate {
                //__________
                dates.append(transaction.date)
                lastDate = transaction.date
            }
        }
        
        return dates
    }
    
    // MARK: -∂ getModifiedDate
    func getModifiedDate(date: String) -> String {
        //__________
        var modifiedDate: String = date
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd MMM yyyy"
        
        if let currentDate = dateFormatter.date(from: currentDateStr) {
            //__________
            if let givenDate = dateFormatter.date(from: date) {
                //__________
                let difference = Calendar.current
                    .dateComponents([.day], from: currentDate, to: givenDate)
                
                if abs(difference.day!) == 0 {
                    //__________
                    modifiedDate = "Today"
                } else if abs(difference.day!) == 1 {
                    //__________
                    modifiedDate = "Yesterday"
                }
            }
        }
        
        return modifiedDate
    }
    
    // MARK: -∂ getTransaction
    func getTransaction(for date: String, number: String) -> [Transaction] {
        //__________
        var transaction: [Transaction] = []
        
        for item in transactions {
            //__________
            if item.date == date && item.card == number {
                transaction.append(item)
            }
        }
        
        if transaction.count == 0 {
            //__________
            lastTransactionID = transaction.last!.id 
        }
        
        return transaction
    }
    /*©---------------------------------©*/
    func getMaxExpenses() -> Float {
        return expenses.map { $0.amount }.max()!
    }
    
    /*©---------------------------------©*/
    func getExpensesDataBasedOnHeight(maxHeight: CGFloat) -> [CGFloat] {
        var heights: [CGFloat] = []
        let max = getMaxExpenses()
        
        for expence in expensesValue {
            let fraction: CGFloat = CGFloat(expence.amount / max)
            let barHeight = maxHeight - CGFloat(fraction * maxHeight)
            
            heights.append(barHeight)
        }
        return heights
    }
    
    /*©---------------------------------©*/
    func selectExpense(expense: Expense) {
        //__________
        if let foundIndex = expensesValue.firstIndex(
            where: { $0.id == expense.id }) {
            //__________
            expensesValue[foundIndex].selected.toggle()
            
            if (foundIndex == lastSelectedExpense) {
                //__________
                lastSelectedExpense = -1
                //__________
            } else if (lastSelectedExpense != -1) {
                //__________
                expensesValue[lastSelectedExpense].selected.toggle()
            }
            
            lastSelectedExpense = foundIndex
        }
    }
    
    /*©---------------------------------©*/
 
}///|>END OF >> Class||


