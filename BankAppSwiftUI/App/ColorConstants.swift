//  ColorConstants.swift
//  BankAppSwiftUI

import SwiftUI

struct ColorConstants {
    // MARK: -∂ Custom-Colors
    /*©---------------------------------©*/
    static let midViolet = Color.init(red: 129/255, green: 11/255, blue: 60/255)
    
    /*©---------------------------------©*/
    static let myColor = Color.init(red: 96/255, green: 8/255, blue: 45/255)
    static let myColor2 = Color.init(red: 0/255, green: 0/255, blue: 0/255)
    static let myColor3 = Color.init(red: 255/255, green: 20/255, blue: 119/255)
    static let myColor4 = Color.init(red: 86/255, green: 8/255, blue: 40/255)
    
    static let appBg1 = Color.init(red: 255/255, green: 189/255, blue: 22/255)
    static let appBg2 = Color.init(red: 122/255, green: 15/255, blue: 229/255)
    static let appBg3 = Color.init(red: 0/255, green: 0/255, blue: 0/255)
    static let appBg4 = Color.init(red: 19/255, green: 22/255, blue: 36/255)
    static let appBg5 = Color.init(red: 29/255, green: 242/255, blue: 255/255)
    /*©---------------------------------©*/
    
    static let primary = Color.init(red: 47/255, green: 16/255, blue: 118/255)
    static let secondary = Color.init(red: 142/255, green: 119/255, blue: 204/255)
    static let imageTint = Color.init(red: 182/255, green: 152/255, blue: 255/255)
    static let imageTint2 = Color.init(red: 120/255, green: 90/255, blue: 201/255)
    static let border = Color.init(red: 66/255, green: 38/255, blue: 135/255)
        
    static let cardBottomLeft = Color.init(red: 107/255, green: 27/255, blue: 255/255)
    static let cardTopRight = Color.init(red: 134/255, green: 65/255, blue: 255/255)
        
    static let graphLine = Color.init(red: 249/255, green: 24/255, blue: 253/255)
    static let graphNumber = Color.init(red: 77/255, green: 49/255, blue: 162/255)
    static let graphBar = Color.init(red: 55/255, green: 25/255, blue: 127/255)
    static let graphBarHighlighted = Color.init(red: 71/255, green: 35/255, blue: 145/255)
    /*©---------------------------------©*/
    static let appBackground = LinearGradient(
        gradient:
            Gradient(
            colors: [myColor, myColor2, myColor3, myColor4]
            ),
        startPoint: .topTrailing,
        endPoint: .bottomLeading)
    
    static let cardBackground2 = LinearGradient(
        gradient:
            Gradient(
            colors: [appBg1, appBg2, appBg3, appBg4, appBg5]
            ),
        startPoint: .topTrailing,
        endPoint: .bottomLeading)
    /*©---------------------------------©*/
    static let cardBackground = LinearGradient(
        gradient: Gradient(colors: [cardTopRight, cardBottomLeft]),
        startPoint: .topTrailing,
        endPoint: .bottomLeading)
            
    static let barBackground = LinearGradient(
        gradient: Gradient(
            colors: [graphBar.opacity(0.2),
                     graphBar.opacity(0.8)]),
        startPoint: .top,
        endPoint: .bottom)
        
    static let barHighlightedBackground = LinearGradient(
        gradient: Gradient(
            colors: [graphBarHighlighted.opacity(0.9),
                     graphBarHighlighted.opacity(0.99)]),
        startPoint: .top,
        endPoint: .bottom)
}
