import SwiftUI

// MARK: - Preview
struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        ContentView(selectedCard: cards[0])//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
    }
}

struct ContentView: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    /// - ©ObservedObject
    @ObservedObject var cardManager = CardManager()
    
    /// - ©State
    @State var currentPage: Int = 0
    @State var selectedCard: Card
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>ZStack||
        //*****************************/
        ZStack {
            // MARK: -∂ Rectangle colored background
            Rectangle()
                .fill(ColorConstants.appBackground)
            
            // MARK: - Child-VStack
            /**---------------------------------*/
            VStack {
                /// - ©TopBarView
                TopBarView()
                
                /// - ©PagerView
                PagerView(pageCount: cards.count, currentIndex: $currentPage) {
                    //__________
                    /// - ©CardView
                    ForEach(cards) { card in
                        CardView(card: card)
                        //__________
                        // MARK: - onTapGesture
                        //@`````````````````````````````````````````````
                            .onTapGesture(perform: {
                                //__________
                                withAnimation(Animation.linear(duration: 0.7)) {
                                    //__________
                                    selectedCard = card
                                    selectedCard.selected = true
                                }
                            })
                        
                        //@-`````````````````````````````````````````````
                    }
                    
                }///|>END OF >> PagerView||
                .frame(height: 240)
                /*©---------------------------------©*/
                
                /// - ©MenuHeaderView
                MenuHeaderView(title: "Transaction",
                               image: "arrow.up.arrow.down")
                
                /// - ©TransactionListView
                TransactionListView(currentIndex: $currentPage,
                                    cardManager: cardManager)
                
                Spacer()// Spaced vertically
            }
            /**---------------------------------*/
            /// - ©If the card is selected show the View
            if (selectedCard.selected) {
                //__________
                CardDetailView(card: $selectedCard, cardManager: cardManager)
            }
            
        }//||END__PARENT-ZStack||
        .edgesIgnoringSafeArea(.all)
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]


