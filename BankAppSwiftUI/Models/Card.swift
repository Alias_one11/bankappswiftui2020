//  Card.swift
//  BankAppSwiftUI

import SwiftUI

struct Card: Identifiable {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var id: String = UUID().uuidString
    /*©---------------------------------©*/
    let number: String
    let type: CardType
    let company: String
    var selected: Bool = false
    /*=============================*/
    
    /// - ©Class methods
    func getLastFourDigits() -> String {
        // - suffix: Returns a subsequence, up to the given maximum length, containing the final elements of the collection.
        String(number.suffix(4))
    }
}
