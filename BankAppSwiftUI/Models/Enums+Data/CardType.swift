//  CardType.swift
//  BankAppSwiftUI

import Foundation
import SwiftUI

enum CardType: String {
    case Silver
    case Gold
    case Platinum
}
