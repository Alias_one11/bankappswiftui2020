import SwiftUI

struct Expense: Identifiable {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    var id: String = UUID().uuidString
    let month: String
    let amount: Float
    var selected: Bool = false
    /*..............................*/
}
