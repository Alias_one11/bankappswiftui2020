import SwiftUI

struct Transaction: Identifiable, Hashable {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var id: String = UUID().uuidString
    /*©---------------------------------©*/
    let date, time, company, service: String
    let card: String
    let amount: Float
    let type: String
    /*=============================*/
}
