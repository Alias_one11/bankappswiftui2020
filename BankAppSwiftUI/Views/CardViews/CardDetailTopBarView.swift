import SwiftUI

struct CardDetailTopBarView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @Binding var card: Card
    /*..............................*/

    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        HStack {
            // MARK: -∂ Drop down with lines
            Button(action: {
                withAnimation {
                    //__________
                    card.selected = false
                }
            }, label: {
                Image(systemName: "multiply")
                    .padding(.all, 20)
                    .font(.system(Font.TextStyle.title2))
                    .shadow(color: Color.black.opacity(0.99), radius: 5, x: 0, y: 10)

            })
            
            // MARK: - Child-> Text
            /**.................................*/
            Text("CARD DETAIL")
                // - ©Sets the spacing, or kerning, between characters.
                .kerning(6.0)
                .font(.system(Font.TextStyle.headline))
                .padding(.leading, 40)
                .shadow(color: Color.black.opacity(0.99), radius: 5, x: 0, y: 10)

            
            Spacer()
            /**.................................*/
            
            // MARK: -∂ Magnifying glass
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                Image(systemName: "slider.vertical.3")
                    .padding(.all, 20)
                    .font(.system(Font.TextStyle.title2))
                    .shadow(color: Color.black.opacity(0.99), radius: 5, x: 0, y: 10)

            })

        }//||END__PARENT-VSTACK||
        .foregroundColor(.white)
        .padding(.top, 64)
        .padding(.bottom, 20)
        .padding(.leading, 20)
        .padding(.trailing, 20)
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]
