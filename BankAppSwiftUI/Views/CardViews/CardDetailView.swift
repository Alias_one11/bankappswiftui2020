import SwiftUI

// MARK: - Preview
struct CardDetailView_Previews: PreviewProvider {
    @ObservedObject static var cardManager = CardManager()
    
    static var previews: some View {
        
        CardDetailView(card: .constant(cards[0]), cardManager: cardManager)//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct CardDetailView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @Binding var card: Card
    @ObservedObject var cardManager: CardManager
    @State var startAnimation: Bool = false
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        ZStack {
            
            Rectangle()
                .fill(ColorConstants.midViolet)
            
            
            ///_CHILD__=>VStack<--ZStack
            /**.................................*/
            VStack {
                //__________
                // MARK: -∂ CardDetailTopBarView
                CardDetailTopBarView(card: $card)
                
                // MARK: - Child-ZStack
                /**.................................*/
                ZStack {
                    //__________
                    GeometryReader { geometry in
                        //__________
                        // MARK: -∂ CardView
                        CardView(card: card)
                            .rotationEffect(startAnimation ? Angle.degrees(90) : Angle.degrees(0))
                            .offset(x: startAnimation ? -geometry.size.width / 2 : 0.0)
                        
                        
                    }//|>END OF >> GeometryReader||
                    .frame(height: 260)
                    //                    .opacity(0)
                    
                    /*©---------------------------------©*/
                    
                    // MARK: -∂ CardInfoView
                    CardInfoView()
                        .padding(.leading, 90)
                        .opacity(startAnimation ? 1 : 0.0)
                        .animation(Animation.easeIn(duration: 0.5).delay(0.8))
                    
                }///|>END OF >> ZStack||
                .padding(.top, 20)
                .padding(.bottom, 20)
                // - ©Hide the content of the view
                /*©---------------------------------©*/
                
                // MARK: -∂ ExpenseView
                ExpenseView(cardManager: cardManager)
                    .padding(.top, 30)
                    .padding(.bottom, 20)
                    .opacity(startAnimation ? 1 : 0.0)
                    .animation(Animation.easeIn(duration: 0.5).delay(0.3))

                
                Spacer()// Vertically spaced
                
                /**.................................*/
                
                
            }///|>END OF >> VStack||
            /**.................................*/
            
        }//||END__PARENT-VSTACK||
        //__________
        // MARK: - onAppear
        //@`````````````````````````````````````````````
        .onAppear(perform: {
            //__________
            withAnimation(Animation.linear(duration: 0.4)) {
                //__________
                startAnimation = true
            }
        })
        
        //@-`````````````````````````````````````````````
        .edgesIgnoringSafeArea(.all)
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]



