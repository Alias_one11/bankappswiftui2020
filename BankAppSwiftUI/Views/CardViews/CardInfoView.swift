import SwiftUI

struct CardInfoView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        
        VStack(alignment: .leading) {
            // MARK: -∂ ProgressView
            ProgressView()
            
            // MARK: - Child-HStack
            /**.................................*/
            HStack(alignment: .firstTextBaseline, spacing: 0) {
                //__________
                Text("$")
                    .font(.system(size: 30, weight: Font.Weight.bold,
                                  design: Font.Design.rounded))
                    .foregroundColor(Color.gray)
                    .shadow(color: Color.black.opacity(0.99), radius: 10, x: 0, y: 10)
                    .padding(.leading, 40)

                Text("5600")
                    .font(.system(size: 40, weight: Font.Weight.bold,
                                  design: Font.Design.rounded))
                    .foregroundColor(Color.white)
                    .shadow(color: Color.black.opacity(0.99), radius: 10, x: 0, y: 10)
                    .padding(.leading, 15)
                
                Text(".90")
                    .font(.system(size: 25, weight: Font.Weight.bold,
                                  design: Font.Design.rounded))
                    .foregroundColor(Color.gray)
                    .shadow(color: Color.black.opacity(0.99), radius: 10, x: 0, y: 10)
                
            }//|>END OF >> HStack||
            
            /**.................................*/
            
            HStack(spacing: 15) {
                //__________
                VStack(alignment: .leading, spacing: 30) {
                    //__________
                    Image(systemName: "creditcard.fill")
                    Image(systemName: "banknote.fill")
                    Image(systemName: "dot.radiowaves.right")
                }///|>END OF >> VStack||
                .font(.system(size: 16, weight: Font.Weight.bold, design: Font.Design.rounded))
                .foregroundColor(Color.white)
                .shadow(color: Color.black.opacity(0.59), radius: 5, x: 0, y: 10)
                .padding(.leading, 40)
                .padding(.top)
                /*©---------------------------------©*/
                
                VStack(alignment: .leading, spacing: 30) {
                    //__________
                    /// - ©Bank Card
                    Text("Bank Card")
                        .kerning(3)
                    /// - ©Bank Account
                    Text("Bank Account")
                        .kerning(3)
                    /// - ©Pay
                    Text("Pay")
                        .kerning(3)
                    
                }///|>END OF >> VStack||
                .font(.system(size: 13, weight: Font.Weight.bold, design: Font.Design.rounded))
                .foregroundColor(.gray)
                .shadow(color: Color.black.opacity(0.99), radius: 5, x: 0, y: 10)
                .padding(.top)

                /*©---------------------------------©*/
                
            }//|>END OF >> HStack||
            
            /*©---------------------------------©*/
            
        }//||END__PARENT-VSTACK||
        
        //............................./
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]
