import SwiftUI

// MARK: - Preview
struct CardView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        CardView(card: cards[0]).padding()
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        .previewLayout(.fixed(width: 440, height: 270))
    }
}

struct CardView: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    let card: Card
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>ZStack||
        //*****************************/
        ZStack {
            
            // MARK: -∂ RoundedRectangle card shape of the card
            RoundedRectangle(cornerRadius: 35)
                .fill(ColorConstants.cardBackground2)
            
            // MARK: - Child-VStack
            /**---------------------------------*/
            VStack {
                //__________
                HStack {
                    //__________
                    Text("")
                    
                    VStack(alignment: .leading) {
                        //__________
                        /// - ©Visa
                        Text(card.company.uppercased())
                            .font(.system(size: 14))
                            .bold()
                            .kerning(2)
                        
                        /// - ©Platinum
                        Text(card.type.rawValue.uppercased())
                            .font(.system(size: 14))
                            .bold()
                            .kerning(2)
                        
                        
                    }///|>END OF >> VStack||
                    .shadow(color: Color.gray.opacity(0.4),
                            radius: 12, x: 15, y: -20)
                    /*©---------------------------------©*/
                    
                    Spacer()// Spaced horizontally
                    
                    /// - ©Visa-Logo
                    Text(card.company.uppercased())
                        .font(.system(size: 24,
                                      weight: Font.Weight.heavy))
                        .italic()
                        .shadow(color: Color.gray.opacity(0.4),
                            radius: 10, x: 10, y: -5)
                    
                    
                }///|>END OF >> HStack||
                
                /*©---------------------------------©*/
                
                Spacer()// Spaced vertically
                
                ///_CHILD__=>HStack<--VSTack
                /**---------------------------------*/
                HStack {
                    //__________
                    /// - ©Asteriks ****
                    ForEach(0..<3) { item in
                        Text("****")
                            .kerning(3)
                        
                        Spacer()
                    }
                    
                    /// - ©Last for digits on the card
                    Text(card.getLastFourDigits())
                        .kerning(3)
                }
                /**---------------------------------*/
                
            }///|>END OF >> VStack||
            .padding(.all, 40)

            /**---------------------------------*/
            
        }//||END__PARENT-ZStack||
        .foregroundColor(.white)
        .padding(.leading, 20)
        .padding(.trailing, 20)
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

