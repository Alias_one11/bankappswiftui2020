import SwiftUI

struct ExpenseBarGraphView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @ObservedObject var cardManager: CardManager
    let height: CGFloat
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        HStack(alignment: .bottom) {
            
            // MARK: -∂ Bar graph
            ForEach(cardManager.expensesValue) { expense in
                //__________
                VStack {
                    //__________
                    //MARK:__=>Dollar amount on top of graph
                    /**.................................*/
                    ZStack {
                        //__________
                        RoundedRectangle(cornerRadius: 10)
                            .fill(ColorConstants.cardBackground2)
                            .opacity(0.29)
                            .shadow(color: Color.black.opacity(0.59), radius: 10, x: 0, y: -10)
                            .frame(width: 65, height: 30)
                            
                        
                        Text("$ \(String(format: "%.2f", expense.amount))")
                            .font(.system(size: 12))
                            .bold()
                            .shadow(color: Color.black.opacity(0.59), radius: 10, x: 0, y: 10)
                            .foregroundColor(.white)
                            
                    }
                    .offset(y: -20)
                    .zIndex(2)
                    .opacity(expense.selected ? 1 : 0.0)
                    /**.................................*/
                    
                    ZStack(alignment: .bottom) {
                        //__________
                        Rectangle()
                            .fill(
                                expense.selected
                                    ? ColorConstants.barHighlightedBackground :
                                    ColorConstants.cardBackground2
                            )
                            .opacity(0.15)
                            .frame(width: 45,
                                   height: getHeightOfBar(
                                    maxHt: height - 30,
                                    amount: expense.amount))
                            // MARK: - onTapGesture
                            //@`````````````````````````````````````````````
                            /// - ©Changing the opacity when a bar is selected
                            .onTapGesture(perform: {
                                //__________
                                withAnimation {
                                    //__________
                                    cardManager.selectExpense(expense: expense)
                                }
                        })
                        //@-`````````````````````````````````````````````
                        
                        // MARK: -∂ Line
                        Line()
                            .stroke(ColorConstants.graphLine,
                                    lineWidth: 2)
                            .frame(width: 45, height: 2)
                            .opacity(expense.selected ? 1 : 0.5)
                            .shadow(color: Color.black.opacity(0.99), radius: 10, x: 0, y: -10)
                        
                    }//|>END OF >> ZStack||
                    
                    
                }///|>END OF >> VStack||
                .frame(width: 50)
                .animation(Animation.linear.delay(0.0))
                
                /*©---------------------------------©*/
            }///|>END OF >> ForEach||
            
        }//||END__PARENT-VSTACK||
        
        //............................./
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
    
    // MARK: -∂ Helper methods
    func getHeightOfBar(maxHt: CGFloat, amount: Float) -> CGFloat {
        //__________
        let max = cardManager.getMaxExpenses()
        let fraction: CGFloat = CGFloat(amount / max)
        let barHeight = CGFloat(fraction * maxHt)
        
        return barHeight
    }
    
}// END: [STRUCT]



