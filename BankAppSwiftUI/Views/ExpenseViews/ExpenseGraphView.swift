import SwiftUI

struct ExpenseGraphView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @ObservedObject var cardManager: CardManager
    @State var startAnimation: Bool = false
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack {
            
            // MARK: -∂ Rectangle graph view
            GeometryReader { geometry in
                //__________
                ZStack {
                    //__________
                    Rectangle()// Lays on top of the Line() view
                        .fill(ColorConstants.cardBackground2)
                        .opacity(0.25)
                        .cornerRadius(10)
                        .blur(radius: 40)
                    
                    // MARK: -∂ GraphBackgroundView
                    GraphBackgroundView()
                    
                    // MARK: -∂ ExpenseBarGraphView
                    ExpenseBarGraphView(cardManager: cardManager,
                                        height: geometry.size.height)
                    
                    
                    ExpenseLineGraph(
                        data:
                            cardManager.getExpensesDataBasedOnHeight(
                                maxHeight: geometry.size.height - 10)
                    )
                    // - ©If true itll animate the graph line from: 0, to: foo...
                    .trim(to: startAnimation ? 1 : 0.0)
                    .stroke(lineWidth: 4)
                    .foregroundColor(ColorConstants.appBg2.opacity(0.39))
                    // - ©Animates the graph at specified duration
                    .animation(Animation.easeIn(duration: 2).delay(0.7))
                    .shadow(color: Color.black.opacity(0.89), radius: 15, x: 0, y: -10)
                    /*©---------------------------------©*/
                    
                    
                }//|>END OF >> ZStack||
                // MARK: - onAppear
                //@`````````````````````````````````````````````
                .onAppear {
                    withAnimation {
                        //__________
                        startAnimation.toggle()// On false by default
                    }
                }
                //@-`````````````````````````````````````````````
                /*©---------------------------------©*/
            }//|>END OF >> GeometryReader||
            .padding(.leading, 20)
            .padding(.trailing, 20)
            
            //MARK:__=>MONTHS .bottom
            /**.................................*/
            HStack {
                //__________
                ForEach(expenses) { expense in
                    //__________
                    Text(expense.month)
                        .font(.caption)
                        .bold()
                        .shadow(color: Color.black.opacity(0.99), radius: 10, x: 0, y: 10)
                        .frame(width: 50)
                }
                
                
            }///|>END OF >> HStack||
            .foregroundColor(.white)
            /**.................................*/
            
        }//||END__PARENT-VSTACK||
        
        //............................./
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]




