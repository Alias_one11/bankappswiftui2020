import SwiftUI

struct ExpenseLineGraph: Shape {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    let data: [CGFloat]
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack {
            
            Text("➡ I AM HERE 🎯!⬅")
                .font(.system(size: 22))
                .foregroundColor(.white)
                .bold()
                .background(Color.black)
            
        }//||END__PARENT-VSTACK||
        
        //............................./
        
    }///-||End Of body||
    
    func path(in rect: CGRect) -> Path {
        //__________
        var path = Path()
        
        var x: CGFloat = 30
        var y: CGFloat = data[0] + 20
        
        path.move(to: CGPoint(x: x, y: y))
        
        var previousPoint = CGPoint(x: x, y: y)
        x += 60
        
        for i in 1..<data.count {
            //__________
            y = data[i] + 20
            let currentPoint = CGPoint(x: x, y: y)
            
            let controlPoint1 = CGPoint(x: previousPoint.x + 45, y: previousPoint.y)
            let controlPoint2 = CGPoint(x: currentPoint.x - 25, y: currentPoint.y)

            path.addCurve(to: currentPoint, control1: controlPoint1, control2: controlPoint2)
            previousPoint = CGPoint(x: x, y: y)
            
            x += 60
        }

        return path
    }
    /*©-----------------------------------------©*/
}// END: [STRUCT]
