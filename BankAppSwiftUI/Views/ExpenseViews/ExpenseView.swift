import SwiftUI

struct ExpenseView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @ObservedObject var cardManager: CardManager
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack {
            //__________
            // MARK: -∂ MenuHeaderView
            MenuHeaderView(title: "Expense", image: "ellipsis")
                .padding(.bottom, 30)

            // MARK: -∂ ExpenseGraphView
            ExpenseGraphView(cardManager: cardManager)
            
        }//||END__PARENT-VSTACK||
        
        //............................./
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]


