import SwiftUI

struct GraphBackgroundView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack {
            //__________
            VStack {
                //__________
                // MARK: - Child-ForEach
                /**.................................*/
                ForEach(0..<3) { _ in
                    // MARK: -∂ Line
                    Line()
                        .stroke(style: StrokeStyle(lineWidth: 0.3, dash: [5]))
                        .frame(height: 0.5)
                    
                    Spacer()
                }
                /**.................................*/
                
            }///|>END OF >> VStack||
            
            Line()
                .stroke(lineWidth: 0.5)
                .frame(height: 0.5)

        }//||END__PARENT-VSTACK||
        .foregroundColor(ColorConstants.secondary)
        //............................./
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

struct Line: Shape {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    
    /*..............................*/
    
    func path(in rect: CGRect) -> Path {
        //__________
        var path = Path()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: rect.width, y: 0))
        
        return path
    }
    
    
}
