import SwiftUI


struct ListHeaderView: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    let title: String
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        HStack {
            Text(title)
                .font(.caption)
                .foregroundColor(ColorConstants.appBg4)
                .padding(.leading, 20)
            
            Spacer()// Spaced horizontally
        }//||END__PARENT-VSTACK||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

