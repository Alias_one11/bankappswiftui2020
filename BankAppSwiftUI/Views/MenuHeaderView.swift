import SwiftUI

// MARK: - Preview
struct MenuHeaderView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        MenuHeaderView(title: "Transaction", image: "arrow.up.arrow.down")//.padding(.all, 100)
        .preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        .previewLayout(.fixed(width: 440, height: 270))
    }
}

struct MenuHeaderView: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    let title, image: String
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>HStack||
        //*****************************/
        HStack(spacing: 8.0) {
            
            Text(title)
                .font(.system(size: 24))
                .bold()
            
            Spacer()// Spaced horizontally
            
            Button(action: {}, label: {
                //__________
                Image(systemName: image)
                    .padding(.all, 20)
            })
            
        }//||END__PARENT-HStack||
        .foregroundColor(.white)
        .padding(.leading, 20)
        .padding(.trailing, 20)

        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

