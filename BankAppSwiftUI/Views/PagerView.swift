import SwiftUI

// MARK: - Preview
//struct PagerView_Previews: PreviewProvider {
//
//    static var previews: some View {
//
//        PagerView<Content: View>(pageCount: 0, currentIndex: .constant(1), content: nil)//.padding(.all, 100)
//        //.preferredColorScheme(.dark)
//        //.previewLayout(.sizeThatFits)
//        //.previewLayout(.fixed(width: 320, height: 640))
//    }
//}

struct PagerView<Content: View>: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    let pageCount: Int
    let content: Content
    /// - ©Binding
    @Binding var currentIndex: Int
    /// - ©GestureState
    /// - A property wrapper type that updates a property while the user performs
    /// - a gesture and resets the property back to its initial state when the gesture ends.
    @GestureState private var translation: CGFloat = 0
    /*=============================*/
    
    /// - ©init
    init(pageCount: Int, currentIndex: Binding<Int>, @ViewBuilder content: () -> Content) {
        //__________
        self.pageCount = pageCount
        self._currentIndex = currentIndex
        self.content = content()
    }
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>GeometryReader||
        //*****************************/
        GeometryReader { geometry in
            
            ///_CHILD__=>ZStack<--GeometryReader
            /**---------------------------------*/
            VStack(spacing: 24) {
                //__________
                HStack(spacing: 0) {
                    content.frame(width: geometry.size.width)
                }
                .frame(width: geometry.size.width, alignment: .leading)
                .offset(x: -CGFloat(currentIndex) * geometry.size.width)
                .offset(x: translation)
                .animation(.interactiveSpring())
                //__________
                // MARK: - .gesture
                //@`````````````````````````````````````````````
                .gesture(
                    DragGesture().updating($translation, body: { (value, state, _) in
                        //__________
                        state = value.translation.width
                    })
                    .onEnded({ value in
                        //__________
                        let offset = value.translation.width / geometry.size.width
                        let newIndex = (CGFloat(currentIndex) - offset).rounded()
                        currentIndex = min(max(Int(newIndex), 0), pageCount - 1)
                    })
                )
                //@-`````````````````````````````````````````````
                /*©---------------------------------©*/
                
                HStack {
                    ForEach(0..<pageCount, id: \.self) { index in
                        //__________
                        Circle()
                            .fill(index == currentIndex ? Color.white : Color.gray)
                            .frame(width: 10, height: 10)
                            .scaleEffect(index == currentIndex ? 0.7 : 0.4)
                    }
                }
            }///|>END OF >> VStack||
            /**---------------------------------*/
            
        }//||END__PARENT-GeometryReader||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

