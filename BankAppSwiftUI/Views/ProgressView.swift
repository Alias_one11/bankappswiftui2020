import SwiftUI

struct ProgressView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        ZStack {
            //__________
            Circle()
                .stroke(lineWidth: 4)
                .fill(ColorConstants.border)
                .frame(width: 50, height: 50)
                .shadow(color: Color.black.opacity(0.99), radius: 10, x: 0, y: 5)

            
            Circle()
                .trim(from: 0, to: 3 / 5)
                .stroke(style: StrokeStyle(lineWidth: 6, lineCap: .round))
                .fill(ColorConstants.graphLine)
                .frame(width: 50, height: 50)
                .shadow(color: Color.black.opacity(0.99), radius: 10, x: 0, y: 5)
                .rotationEffect(Angle.degrees(-90))
            
            Text("3/5")
                .font(.system(size: 16))
                .bold()
                .foregroundColor(.white)
            
        }//||END__PARENT-VSTACK||
        .padding(.leading, 37)
        
        //............................./
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]
