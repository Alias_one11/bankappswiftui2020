import SwiftUI

// MARK: - Preview
struct TopBarView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        TopBarView().padding()
        .preferredColorScheme(.dark)
        .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
    }
}

struct TopBarView: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>HStack||
        //*****************************/
        HStack {
            
            // MARK: -∂ Home
            Button(action: {
                
            }, label: {
                Image(systemName: "line.horizontal.3")
                    .padding(.all, 20)
            })
            
            Text("HOME")
            Spacer()// Spaced horizontally
            
            // MARK: -∂ Magnifying glass
            Button(action: {
                
            }, label: {
                Image(systemName: "magnifyingglass")
                    .padding(.all, 20)

            })
            
            
        }//||END__PARENT-HStack||
        .foregroundColor(.white)
        .padding(.top, 64)
        .padding(.bottom, 20)
        .padding(.leading, 20)
        .padding(.trailing, 20)
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

