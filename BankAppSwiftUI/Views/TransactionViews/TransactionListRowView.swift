import SwiftUI

struct TransactionListRowView: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    let transaction: Transaction
    let isLast: Bool
    /*=============================*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        VStack(spacing: 8.0) {
            //__________
            // MARK: - Child-HStack
            /**---------------------------------*/
            HStack {
                //__________
                ///_CHILD__=>ZStack<--HStack
                /**---------------------------------*/
                ZStack {
                    //__________
                    Circle()
                        .fill(ColorConstants.midViolet)
                        .frame(width: 50, height: 50)
                    
                        /// - Adds a linear gradient to the apple logo SFSymbol
                        LinearGradient(gradient:
                                        Gradient(colors: [
                                            ColorConstants.appBg1, ColorConstants.appBg5,
                                            ColorConstants.appBg2, ColorConstants.appBg4, ColorConstants.appBg2
                                        ]),
                                       startPoint: .topTrailing, endPoint: .bottomLeading)
                            // MARK: -∂ SFSymbol
                            .mask(Image(systemName: "applelogo")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                  
                            ).frame(width: 40, height: 20, alignment: .center)
                    
                }//|>END OF >> ZStack||
                /**---------------------------------*/
                
                ///_CHILD__=>VStack<--HStack
                VStack(alignment: .leading) {
                    //__________
                    /// - ©Service
                    Text(transaction.service)
                        .foregroundColor(.white)
                        .fontWeight(.heavy)
                    
                    /// - ©Type
                    Text(transaction.type)
                        .font(.caption)
                        .foregroundColor(Color.black.opacity(0.6))
                        .fontWeight(.bold)
                    
                }//|>END OF >> VStack||
                
                /**---------------------------------*/
                
                Spacer()// Spaced horizontally
                
                // MARK: -∂ .trailing components
                ///_CHILD__=>VStack<--HStack
                VStack(alignment: .trailing) {
                    //__________
                    /// - ©Service
                    Text("- $\(String(format: "%.2f", transaction.amount)) USD")
                        .foregroundColor(.white)
                        .fontWeight(.heavy)
                    
                    /// - ©Type
                    Text(transaction.time)
                        .font(.caption)
                        .foregroundColor(Color.black.opacity(0.6))
                        .fontWeight(.bold)
                    
                }//|>END OF >> VStack||
                
                /**---------------------------------*/
                
            }//|>END OF >> HStack||
            .padding(.leading, 20)
            .padding(.trailing, 20)
            /**---------------------------------*/
            
            Divider()// Dividing vertically
                .background(ColorConstants.midViolet)
                .opacity(isLast ? 0.0 : 1.0)
                .padding(.leading, 77)
                .padding(.bottom, 8)
            
        }//||END__PARENT-VSTACK||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

