import SwiftUI

// MARK: - Preview
//struct TransactionListView_Previews: PreviewProvider {
//
//    static var previews: some View {
//
//        TransactionListView()//.padding(.all, 100)
//        //.preferredColorScheme(.dark)
//        //.previewLayout(.sizeThatFits)
//        //.previewLayout(.fixed(width: 320, height: 640))
//        // The preview below is for like a card
//        .previewLayout(.fixed(width: 440, height: 270))
//    }
//}

struct TransactionListView: View {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    /// - ©Binding
    @Binding var currentIndex: Int
    
    /// - ©ObservedObject
    @ObservedObject var cardManager: CardManager
    /*=============================*/
    
    /// - ©Helper methods
    /*©---------------------------------©*/
    func getTransactions(date: String) -> [Transaction] {
        cardManager.getTransaction(for: date, number: cards[currentIndex].number)
    }
    
    func getListHeader() -> [String] {
        cardManager.getUniqueDates(for: cards[currentIndex].number)
    }
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //*****************************/
        ScrollView {
            //__________
            LazyVStack {
                
                ForEach(getListHeader(), id: \.self) { date in
                    //__________
                    ListHeaderView(title: cardManager.getModifiedDate(date: date))
                    
                    ForEach(getTransactions(date: date), id: \.self) { item in
                        //__________
                        TransactionListRowView(
                            transaction: item,
                            isLast: cardManager.lastTransactionID == item.id
                        )
                        //__________
                    }
                }
                
            }
        }//||END__PARENT-VSTACK||
        
        //*****************************/
        
    }///-||End Of body||
    /*©-----------------------------------------©*/
}// END: [STRUCT]

